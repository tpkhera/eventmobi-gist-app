import React from 'react'

import './style.scss'

const Header = ({ children }) => {
  return (
    <h1 className="app-header">{children}</h1>
  )
}

export default Header

import React, { useState } from 'react'

import { Header, Loader } from 'components'
import { GistListContainer, GistSearchContainer } from 'containers'
import { requestHelper } from 'helpers'
import './App.scss'

const App = () => {
  const [userName, setUserName] = useState('')
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)
  const [gistList, setGistList] = useState(null)

  const getGistsForUser = async (userName) => {
    setLoading(true)
    try {
      const gistList = await requestHelper.fetchGists(userName)
      const gistListWithForksPromises = gistList.map(async (gist) => {
        const forksData = await requestHelper.fetchGistForks(gist.id)
        return { ...gist, forksData }
      })
      const gistListWithForks = await Promise.all(gistListWithForksPromises)
      setGistList(gistListWithForks)
    } catch (error) {
      setError(error)
    } finally {
      setLoading(false)
    }
  }

  return (
    <div className='app-container'>
      <Header>Gisto!</Header>
      <GistSearchContainer
        userName={userName}
        setUserName={setUserName}
        getGistsForUser={getGistsForUser}
      />
      {loading && <Loader />}
      <GistListContainer gistList={gistList} />
    </div>
  )
}

export default App

import React from 'react'

import './style.scss'

const GistListContainer = ({ gistList }) => {
  if (!gistList) return null
  return (
    <div className='gist-list-container'>
      {
        gistList.map((gist, idx) => <GistListRow key={idx} gist={gist} />)
      }
    </div>
  )
}

// File icons can be obtained by creating a mapping between mimetypes/languages
// and icon fonts like fontawesome or even raw images from something
// like https://github.com/pasnox/oxygen-icons-png/tree/master/oxygen/16x16/mimetypes
// I felt creating an exhaustive map would go beyond the scope of the assignment
const getIconFromMimetype = (mimetype) => {
  const mimeFontMap = {
    "text/plain": "far fa-file-alt",
    "text/html": "far fa-file-code",
  }

  return mimeFontMap[mimetype] || 'far fa-file'
}

const GistListRow = ({ gist }) => {
  const [, { filename, type }] = Object.entries(gist.files)[0]
  const fileName = filename
  const fileIcon = getIconFromMimetype(type)

  return (
    <div className='gist-list-row'>
      <GistFilename fileName={fileName} fileIcon={fileIcon} />
      <GistForksRow forksData={gist.forksData} />
    </div>
  )
}

const GistFilename = ({ fileName, fileIcon }) => {
  return (
    <div className='gist-filename'><i className={fileIcon}></i><span>{fileName}</span></div>
  )
}

const GistForksRow = ({ forksData }) => {
  if (!forksData || forksData.length < 1) return null
  return (
    <span className='gist-forks'>
      <i className='fork-icon fas fa-code-branch'></i>
      {
        forksData.length > 0 &&
        forksData.slice(0, 3).map((fork, idx) =>
          <UserAvatar
            key={idx}
            avatarUrl={fork.owner.avatar_url}
            forkUrl={fork.html_url}
            name={fork.owner.login}
          />)
      }
    </span>
  )
}

const UserAvatar = ({ forkUrl, name, avatarUrl }) => {
  return (
    <a href={forkUrl} title={name}>
      <img alt='avatar' className='user-avatar' src={avatarUrl} />
    </a>
  )
}

export default GistListContainer

import React from 'react'

import './style.scss'

const GistSearchContainer = ({ userName, setUserName, getGistsForUser }) => {
  return (
    <div className='gist-search-container'>
      <input
        type='text'
        className='gist-search-input'
        placeholder='Github username'
        value={userName}
        onChange={ev => setUserName(ev.target.value)}
      />
      <i
        className='search-icon fas fa-search'
        onClick={() => getGistsForUser(userName)}
      />
    </div>
  )
}

export default GistSearchContainer

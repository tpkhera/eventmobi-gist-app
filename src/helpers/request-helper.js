const API_CONSTANTS = {
  GET_GIST: (userName) => `https://api.github.com/users/${userName}/gists`,
  GET_GIST_FORK: (gistId) => `https://api.github.com/gists/${gistId}/forks?per_page=3&page=1`
}

const fetchGithubApi = async (url) => {
  const response = await fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/vnd.github.v3+json'
    }
  })
  const responseJson = await response.json()
  if (response.status !== 200) {
    throw new Error(responseJson.message)
  }
  return responseJson
}

const fetchGists = (userName) => {
  return fetchGithubApi(API_CONSTANTS.GET_GIST(userName))
}

const fetchGistForks = (gistId) => {
  return fetchGithubApi(API_CONSTANTS.GET_GIST_FORK(gistId))
}

export default {
  fetchGists,
  fetchGistForks
}